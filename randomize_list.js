/*npm install readline-sync*/
const read_line = require('readline-sync');

let items = [];

function obtain_items(items){
	let quit = false;
	let item;
	console.log('Press \'q\' to quit\n');
	while(quit !== true){
		item = read_line.question('Item: ');
		item === 'q' ? quit = true : items.push(item);
	}
	return items;
}

function print_all_items(items){
	console.log('\nPrinting reordered items: ');
	items.forEach(
		(item, index) => {
			console.log(index + '. ' + item);
		}
	);
}

function randomize_items(items){
	let len =  items.length;
	let taken = [];
	let new_items = [];
	let same_order = true;

	while(same_order){
		new_items = create_new_items(taken, len, items);
		same_order = check_if_arrays_in_same_order(items, new_items);
	}
	return new_items;
}
function get_random_index(taken, len){
	let random_index;
	let not_taken = true;

	while(not_taken){
		random_index = Math.floor(Math.random()*len);
		if(!taken.includes(random_index)){
			taken.push(random_index)
			let results = {'taken':taken, 'index':random_index};
			return results;
			not_taken = false;
		}
	}
}

function create_new_items(taken, len, items){
	let new_items = []
	for(let i = 0; i < len; i++){
		let results = get_random_index(taken, len);
		new_items.push(items[results['index']]);
	}
	return new_items;
}

function check_if_arrays_in_same_order(array1, array2){
	let len = array1.length;
	let counter = 0;
	let result;

	for(let i = 0; i < len; i++){
			if(array1[i] === array2[i]){
				counter++;
			}
	}
	len === counter ? result = true : result = false;
	return result;
}

items = obtain_items(items);
print_all_items(randomize_items(items));
