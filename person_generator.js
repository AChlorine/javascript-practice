const fs = require('fs');

function obtain_data(path){
  let placeholder = fs.readFileSync(path, 'utf8');
  let array = placeholder.toString().split('\n');
  return array;
}

function mix_names_with_surnames(data, sexes, size){
  let names = [];
  let size_of_names = data['female_names'].length - 1; //Due to the fact that arrays begin from 0
  let size_of_surnames = data['surnames'].length - 1;
  let random_name = 0;
  let random_surname = 0;

  for(var i = 0; i < size; i++){
    random_name = Math.round(Math.random()*size_of_names);
    random_surname = Math.round(Math.random()*size_of_surnames);

    sexes[i] === 0 ? name = `${data['female_names'][random_name]} ${data['surnames'][random_surname]}`
    :
    name = `${data['male_names'][random_name]} ${data['surnames'][random_surname]}`
    names.push(name);
  }
  return names;
}

function generate_sex(size){
  let male_or_female = 0;
  let sexes = [];
  for(var i = 0; i < size; i++){
    male_or_female = Math.round((Math.random()*1)); //Written like this for clarity
    sexes.push(male_or_female);
  }
  return sexes;
}
function generate_ages(size){
  let ages = [];
  const generate_age = () => Math.random() * (60 - 18) + 18; //This is a formula to generate a random number in a given range
  for(let i = 0; i < size; i++){
    ages.push(~~generate_age()); //Double bitwise NOT, which rounds numbers
  }
  return ages;
}
function generate_subjects(data, size){
  let subjects = [];
  for(let i = 0; i < size; i++){
    random_subject = ~~(Math.random()*data['subjects'].length);
    subject = data['subjects'][random_subject];
    subjects.push(subject);
  }
  return subjects;
}
function create_persons(names, sexes, ages, subjects){
  let persons = [];
  for(let i = 0; i < names.length; i++){
    let person = {
      'name': names[i],
      'sex': sexes[i],
      'age': ages[i],
      'subject': subjects[i]
    };
    persons.push(person);
  }
  return persons;
}

files = {
  'females': 'female_first_names.txt',
  'males': 'male_first_names.txt',
  'surnames': 'surnames.txt',
  'subjects': 'subjects.txt'
};

data = {
  'female_names' : obtain_data(files['females']),
  'male_names' : obtain_data(files['males']),
  'surnames' : obtain_data(files['surnames']),
  'subjects' : obtain_data(files['subjects'])
};

let size = ~~(Math.random() * (20 - 1) + 1);
let sexes = generate_sex(size);
let names = mix_names_with_surnames(data, sexes, size);
let ages = generate_ages(size);
sexes = sexes.map(sex => sex == 0 ? 'Female' : 'Male')
let subjects = generate_subjects(data, size);
let persons = create_persons(names, sexes, ages, subjects)
console.log("Sample size: " + size);

for(let item of persons){
  console.log(item)
}
